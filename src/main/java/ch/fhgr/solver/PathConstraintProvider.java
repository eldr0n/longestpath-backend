package ch.fhgr.solver;

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;
import ch.fhgr.domain.Domicile;
import ch.fhgr.domain.Visit;

public final class PathConstraintProvider implements ConstraintProvider {
    @Override
    public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
        return new Constraint[]{
                distanceToPreviousStandstill(constraintFactory),
                distanceFromLastVisitToDomicile(constraintFactory),
                neighbourConflict(constraintFactory),
                lastVisitDomicileConflict(constraintFactory)
        };
    }

    // hard constraints
    private Constraint neighbourConflict(ConstraintFactory constraintFactory) {
        return constraintFactory
                .from(Visit.class)
                .filter(visit -> visit.getDistanceFromPreviousStandstill() == 0)
                .penalize("Neighbour conflict", HardSoftLongScore.ONE_HARD);
    }

    private Constraint lastVisitDomicileConflict(ConstraintFactory constraintFactory) {
        return constraintFactory.from(Visit.class)
                .ifNotExists(Visit.class, Joiners.equal(visit -> visit, Visit::getPreviousStandstill))
                .join(Domicile.class)
                .filter((visit, domicile) -> visit.getDistanceTo(domicile) == 0)
                .penalize("last visit -> domicile conflict",
                        HardSoftLongScore.ofHard(100));
    }

    // soft constraints
    private Constraint distanceToPreviousStandstill(ConstraintFactory constraintFactory) {
        return constraintFactory.from(Visit.class)
                .rewardLong("Distance to previous standstill",
                        HardSoftLongScore.ONE_SOFT,
                        Visit::getDistanceFromPreviousStandstill);
    }

    private Constraint distanceFromLastVisitToDomicile(ConstraintFactory constraintFactory) {
        return constraintFactory.from(Visit.class)
                .ifNotExists(Visit.class, Joiners.equal(visit -> visit, Visit::getPreviousStandstill))
                .join(Domicile.class)
                .rewardLong("Distance from last visit to domicile",
                        HardSoftLongScore.ONE_SOFT,
                        Visit::getDistanceTo);
    }
}

package ch.fhgr.rest;

import ch.fhgr.bootstrap.DataGenerator;
import ch.fhgr.domain.*;
import ch.fhgr.domain.location.Location;

import ch.fhgr.socket.VisitsSocket;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.optaplanner.core.api.solver.SolverJob;

import org.optaplanner.core.api.solver.SolverManager;
import org.optaplanner.core.config.constructionheuristic.ConstructionHeuristicPhaseConfig;
import org.optaplanner.core.config.constructionheuristic.ConstructionHeuristicType;
import org.optaplanner.core.config.localsearch.LocalSearchPhaseConfig;
import org.optaplanner.core.config.localsearch.LocalSearchType;
import org.optaplanner.core.config.localsearch.decider.acceptor.LocalSearchAcceptorConfig;
import org.optaplanner.core.config.localsearch.decider.forager.LocalSearchForagerConfig;
import org.optaplanner.core.config.phase.PhaseConfig;
import org.optaplanner.core.config.solver.SolverConfig;
import org.optaplanner.core.config.solver.SolverManagerConfig;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class PathRessource {

    DataGenerator zone1;
    DataGenerator zone2;
    DataGenerator zone3;
    PathProblem problem;
    SolverJob<PathProblem, Long> job;

    SolverConfig solverConfig = SolverConfig.createFromXmlResource("solverConfig.xml");
    SolverManager<PathProblem, Long> solverManager = SolverManager.create(solverConfig,
            new SolverManagerConfig());

    public PathRessource() throws IOException, InterruptedException {
        zone1 = new DataGenerator(1);
        zone2 = new DataGenerator(2);
        zone3 = new DataGenerator(3);
        problem = new PathProblem();
        problem.setName("longest path");
    }

    @POST
    @Path("/solve")
    public SolverJob<PathProblem, Long> solve(String body) throws JsonProcessingException {
        JsonNode json = getRequestBody(body);
        Long id = json.get("id").asLong();
        int zone = json.get("zone").asInt();
        switch (zone) {
            case 1 -> {
                problem.setDomicile(zone1.getDomicile());
                problem.setLocationList(zone1.getLocationList());
                problem.setVisitList(zone1.getVisitList());
            }
            case 2 -> {
                problem.setDomicile(zone2.getDomicile());
                problem.setLocationList(zone2.getLocationList());
                problem.setVisitList(zone2.getVisitList());
            }
            case 3 -> {
                problem.setDomicile(zone3.getDomicile());
                problem.setLocationList(zone3.getLocationList());
                problem.setVisitList(zone3.getVisitList());
            }
        }

        problem.setId(id);
        job = solverManager.solveAndListen(id, this::getProblem, this::save);
        return job;
    }

    @POST
    @Path("/stop")
    public Response stop(String body) throws JsonProcessingException {
        JsonNode json = getRequestBody(body);
        Long id = json.get("id").asLong();
        if (solverManager.getSolverStatus(id).toString().equals("SOLVING_ACTIVE")) {
            solverManager.terminateEarly(id);
        }
        return Response.noContent().build();
    }


    @POST
    @Path("/options")
    public Response setOptions(String body) throws JsonProcessingException {
        JsonNode json = getRequestBody(body);

        long terminationTime = json.get("terminationTime").asLong();

        List<PhaseConfig> phaseConfigs = new ArrayList<>();

        if (json.has("heuristicType")) {
            ConstructionHeuristicType constuctionHeursitc = ConstructionHeuristicType.valueOf(json.get("heuristicType").asText());
            ConstructionHeuristicPhaseConfig heurConf = (ConstructionHeuristicPhaseConfig) solverConfig.getPhaseConfigList().get(0).copyConfig();
            heurConf.setConstructionHeuristicType(constuctionHeursitc);
            phaseConfigs.add(heurConf);
        }

        if (json.has("localSearchType")) {
            LocalSearchPhaseConfig searchConf = new LocalSearchPhaseConfig();
            if (json.get("localSearchType").asText().equals("SIMULATED_ANNEALING")) {
                LocalSearchAcceptorConfig acceptConf = new LocalSearchAcceptorConfig();
                acceptConf.setSimulatedAnnealingStartingTemperature("0hard/0100000soft");
                acceptConf.setEntityTabuSize(5);

                LocalSearchForagerConfig foragerConf = new LocalSearchForagerConfig();
                foragerConf.setAcceptedCountLimit(1);

                searchConf.setAcceptorConfig(acceptConf);
            } else {
                LocalSearchType searchType = LocalSearchType.valueOf(json.get("localSearchType").asText());
                searchConf.setLocalSearchType(searchType);
            }
            phaseConfigs.add(searchConf);
        }

        try {
            SolverConfig newConfig = new SolverConfig(solverConfig);

            newConfig.getTerminationConfig().setMinutesSpentLimit(terminationTime);
            if (json.has("heuristicType") && json.has("localSearchType")) {
                newConfig.setPhaseConfigList(phaseConfigs);
            }
            solverManager = SolverManager.create(newConfig,
                    new SolverManagerConfig());
            return Response.ok(json).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.serverError().build();
        }
    }

    protected void save(PathProblem problemo) {
        problem.setVisitList(problemo.getVisitList());
        problem.setScore(problemo.getScore());
        try {
            VisitsSocket.sendMessage(jsonBuilder(), problem.getId().toString());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    protected PathProblem getProblem(Long id) {
        if (problem.getId().equals(id)) {
            return problem;
        }
        return new PathProblem();
    }

    private List<String> parseVisitToGeo() throws IOException, InterruptedException {
        Location domicile = problem.getDomicile().getLocation();
        List<String> visits = new ArrayList<>();
        List<Location> locations = new ArrayList<>();
        List<Location> prevLocations = new ArrayList<>();

        for (Visit visit : problem.getVisitList()) {
            Location location = visit.getLocation();
            Location prevLocation = visit.getPreviousStandstill().getLocation();
            locations.add(location);
            prevLocations.add(prevLocation);
            String geometry = HTTPClient.getGeometry(prevLocation, location);
            visits.add(geometry);
        }

        locations.removeAll(prevLocations);
        String geometry = HTTPClient.getGeometry(locations.get(0), domicile);
        visits.add(geometry);
        return visits;
    }

    private JsonNode getRequestBody(String body) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(body);
    }

    private String jsonBuilder() throws IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();

        ArrayNode arrayNode = objectNode.putArray("visits");
        List<String> geoVisitsList = parseVisitToGeo();
        for (String geoVisit : geoVisitsList) {
            arrayNode.add(geoVisit);
        }

        objectNode.put("distance", problem.getDistance());
        objectNode.put("score", problem.getScore().toString());

        return objectNode.toString();

    }
}

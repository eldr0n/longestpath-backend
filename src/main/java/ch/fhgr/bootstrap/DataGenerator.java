package ch.fhgr.bootstrap;

import ch.fhgr.domain.Domicile;
import ch.fhgr.domain.Visit;
import ch.fhgr.domain.location.Location;
import ch.fhgr.domain.location.RoadLocation;
import ch.fhgr.rest.HTTPClient;

import javax.enterprise.context.ApplicationScoped;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

@ApplicationScoped
public class DataGenerator {

    List<Location> locationList = new ArrayList<>();
    List<Visit> visitList = new ArrayList<>();
    Domicile domicile = new Domicile();
    Double[][] adjMatrix;
    Double[][] coordsMatrix;

    public  DataGenerator(){}

    public DataGenerator(int zone) throws IOException, InterruptedException {
        generateData(zone);
    }

    public void generateData(int zone) throws IOException, InterruptedException {
        adjMatrix = parseXML(System.getProperty("user.dir")+"/../src/data/adj_matrix_z" + zone +".csv");
        coordsMatrix = parseXML(System.getProperty("user.dir")+"/../src/data/coordinates_z" + zone +".csv");
        for(int i = 0; i < coordsMatrix.length; i++ ) {
            Double lat = coordsMatrix[i][0];
            Double lon = coordsMatrix[i][1];
            Location location = new RoadLocation(i, lat, lon);
            location.setName("n"+i);
            locationList.add(location);
        }

        for (int i = 0;i < locationList.size(); i++) {
            Map<RoadLocation, Double> map = new LinkedHashMap<>(locationList.size());
            RoadLocation location = (RoadLocation) locationList.get(i);
            Double[] neighbours = adjMatrix[i];
            for (int j = 0;j<neighbours.length;j++) {
                RoadLocation otherLocation = (RoadLocation) locationList.get(j);
                if (neighbours[j] == 0.0 ) {
                    map.put(otherLocation, 0.0);
                } else {
                    map.put(otherLocation, HTTPClient.getDistance(location,  otherLocation));
                }
            }
            location.setTravelDistanceMap(map);

            if (i < 1) {
                domicile.setId(location.getId());
                domicile.setLocation(location);
            } else {
                Visit visit = new Visit();
                visit.setId(location.getId());
                visit.setLocation(location);
                visitList.add(visit);
            }
        }
    }

    public List<Location> getLocationList() {
        return locationList;
    }

    public List<Visit> getVisitList() {
        return visitList;
    }

    public Domicile getDomicile() {
        return domicile;
    }

    private Double[][] parseXML(String path) throws IOException {
        String thisLine;
        FileInputStream fis = new FileInputStream(path);
        DataInputStream myInput = new DataInputStream(fis);
        int i = 0;
        Double[][] data = new Double[0][];
        while ((thisLine = myInput.readLine()) != null) {
            ++i;//increment the line count when new line found

            Double[][] newData = new Double[i][2];//create new array for data

            String[] strAr = thisLine.split(",");//get contents of line as an array
            Double[] doubleAr = new Double[strAr.length];
            for (int j = 0; j < strAr.length; j++) {
                doubleAr[j] = Double.parseDouble(strAr[j]);
            }
            newData[i - 1] = doubleAr;//add new line to the array

            System.arraycopy(data, 0, newData, 0, i - 1);//copy previously read values to new array
            data = newData;//set new array as csv data
        }
        return data;
    }
}

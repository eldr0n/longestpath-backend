package ch.fhgr.socket;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.Session;

@ServerEndpoint("/api/visits/{id}")
@ApplicationScoped

public class VisitsSocket {

    public static Map<String, Session> sessions = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("id") String id) {
        if (id.equals("null")) {
            return;
        }
        sessions.put(id, session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("id") String id) {
        sessions.remove(id);
    }

    @OnMessage
    public static void sendMessage(String message, @PathParam("id") String id) {
        broadcast(message, id);
    }

    private static void broadcast(String message, String id) {
        sessions.get(id).getAsyncRemote().sendObject(message, result ->  {
            if (result.getException() != null) {
                System.out.println("unable to send message: " + result.getException());
            }
        });
    }
}

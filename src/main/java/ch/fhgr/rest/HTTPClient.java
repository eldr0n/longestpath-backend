package ch.fhgr.rest;

import ch.fhgr.domain.location.Location;
import ch.fhgr.domain.location.RoadLocation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HTTPClient {

    public static String getGeometry(Location start, Location end) throws IOException, InterruptedException {
        JsonNode jsonNode = fetchWaypoints(start, end);
        return jsonNode.get("routes").get(0).get("geometry").asText();
    }

    public static Double getDistance(RoadLocation location, RoadLocation otherLocation) throws IOException, InterruptedException {
        JsonNode jsonNode = fetchWaypoints(location, otherLocation);
        return jsonNode.get("routes").get(0).get("distance").asDouble();
    }

    private static JsonNode fetchWaypoints(Location start, Location end) throws IOException, InterruptedException {
        Double lat1 = start.getLatitude();
        Double long1 = start.getLongitude();
        Double lat2 = end.getLatitude();
        Double long2 = end.getLongitude();
        HttpClient client = HttpClient.newHttpClient();
        String url = "http://localhost:6000/route/v1/foot/";
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + long1 + "," + lat1 + ";" + long2 + "," + lat2))
                .header("accept", "application/json")
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readTree(response.body());
    }
}

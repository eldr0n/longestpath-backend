package ch.fhgr.rest;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class CorsFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext,
                       ContainerResponseContext responseContext) throws IOException {
        if (requestContext.getHeaderString("Origin") != null) {
            responseContext.getHeaders().add("Access-Control-Allow-Origin",
                    requestContext.getHeaderString("Origin"));

            responseContext
                    .getHeaders()
                    .add("Access-Control-Expose-Headers",
                            "Content-Type, Location, Link, Accept, Allow, Retry-After");

            responseContext.getHeaders().add("Access-Control-Allow-Headers",
                    "Origin, Content-Type, Accept");

            responseContext.getHeaders().add(
                    "Access-Control-Allow-Credentials", "true");

            responseContext.getHeaders().add("Access-Control-Allow-Methods",
                    "OPTIONS, HEAD, GET, POST, PUT, PATCH, DELETE");
        }
    }
}
